import React from 'react';
import { View, Text, StyleSheet, TextInput} from 'react-native'
import { getMeteoByCity } from './../API/API.js'

/**
 * Component principal. Permet de rechercher une ville et d'afficher les informations météo.
 */
class Home extends React.Component {
    constructor(props) {
        super(props)
        this.searchedCity = ''
        this.state = { meteo: null }
    }

    /**
     * Fonction utilisée pour mettre la valeur saisie dans la variable searchedCity.
     * @param {*} pCity La ville saisie dans le champs texte.
     */
    setMeteoCity(pCity) {
        this.searchedCity = pCity
    }

    /**
     * Fonction pour lancer une recherche sur la ville saisie. Appelle l'API.
     */
    searchMeteoCity() {
        getMeteoByCity(this.searchedCity).then(res => {
            console.log(res.data)
        }).catch(err => {
            console.log(err)
        })
    }

    render() {
        return (
            <View style={styles.content}>
                <View style={styles.search}>
                    <TextInput style={styles.textInput} placeholder="Saisissez ici le nom de la ville" onChangeText={(text => this.setMeteoCity(text))}></TextInput>
                </View>
                <Text>Home</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1
    },
    search: {
        flex: 2,
        justifyContent: 'center',
    },
    textInput: {
        marginRight: 10,
        marginLeft: 10,
        marginTop: 10,
        marginBottom: 10,
        padding: 5,
        fontSize: 18,
        backgroundColor: 'white'
    },

})
export default Home



