## Comment lancer le projet
### 1 - Clonez le projet
	git clone https://gitlab.com/ThomasJamme/meteodirect.git

### 2 - Placez vous dans le projet et installez les dépendances
	npm install

### 3 - Lancez le projet en lancant le serveur expo
	npm start
