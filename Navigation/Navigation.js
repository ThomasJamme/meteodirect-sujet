import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Home from '../Components/Home'

const MeteoNavigator = createStackNavigator({
    // Page d'accueil permettant d'afficher les résultats météo pour une ville donnée.
    Home: {
        screen: Home,
        navigationOptions: {
            title: 'Meteo direct'
        }
    },
})

export default createAppContainer(MeteoNavigator)
