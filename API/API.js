import axios from 'axios'

/**
 * Fonction permettant d'appeler l'API qui retourne les informations météo d'une ville.
 * @param {*} pCity La ville dont les informations sont demandées.
 * @returns Des informations météo.
 */
export function getMeteoByCity(pCity) {
    const API_KEY = '444c6392c1ce282d563c06b46068e558'
    const url = 'https://api.openweathermap.org/data/2.5/weather'
    return new Promise((resolve, reject) => {
        return axios.get(url, {params: {
            q: pCity,
            appid: API_KEY,
            units: 'metric',
            lang: 'fr'
        }}).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        });
    });
}

// Adresse pour obtenir la météo pour les jours à venir :
// https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&exclude=hourly,minutely,current&appid={API key}
// Détail des params à passer : 
// {params: {lat: {lat},lon: {lon},exclude: 'hourly,minutely,current',appid: {API key},units: 'metric',lang: 'fr'}
